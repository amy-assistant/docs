# Questions
### Was ist Amy?

Amy ist eine Messaging App (Layout like WhatApp, Telegram) wo sich der User mit allen seinen Nachrichten Services anmelden kann. Amy analysiert dann die Nachrichten periodisiert diese Nachrichten schließlich. 

### Wie ist Amy Aufgebaut?

Amy hat einen Server und einen Client. Ohne den Server funktioniert der Client nicht. Der Server hat einen **Core** welcher alle Module startet. Er verwaltet (CRUD) auch ***Plugins*** und ***User***. Der **Worker** verwaltet die ***Nachrichten***, speichert die erstellten Nachrichten in die NachrichtenDB. Wo dann die **API** sie ausließt und dem ***Client schickt***, welche über Websockets verbunden sind. 

### Welche Technologien Werdern für Amy verwendet?

**Languages:**

* JavaScript
* Python
* Rust

**Frameworks & Modules:**

* VueJS
* Express
* ExpressWebSockets??
* MongoDB
* RabbitMQJS

**Databases:**

* MongoDB
* RabbitMQ

### Was kann Amy?
Amy verwalten macht verschiede Nachrichten Diensten zu einem. Konversationen von allen Apps sind in einem Chat (Instagram, WhatApp, usw)

## Amy Structure New
/images/Amy-Structure.svg

## Amy Structure Old 

/images/Amy-Structure-old.jpg
