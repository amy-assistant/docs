# Userstories
* User kann sich registrieren.
* User kann sich anmelden.
* User kann von beliebigen Chat einen auswählen.
* User kann mit verschieden Platformen zum Chatten auswählen.
* User kann verschiede Plattformen zu einem User hinzufügen.
* User kann sich bei Plattformen anmelden.
* User kann Nachrichten empfangen.
* User kann Nachrichten schreiben.
* Kann Kontakt blockieren.
* Kann Chat löschen. 
* Kann "App-Notification-Punktfarbe" ändern.
* User kann chat archivieren.
* User kann Profilbilder sehen.
* Kann Profilbild ändern.
* User hat einen first Setupscreen.

