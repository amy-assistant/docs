# Worker

## Leitgedanke

Ein optionals Module welches sich mit der Queue und der Datenbank verbindet um die Messages, die über die Queue gesendet werden um Daten zu erweitern. 

<!--
Der Worker wurde in Python umgesetzt da python viele libraries zum Thema machine learning hat.
-->


## Stateless

Im Worker selber werden keine Daten gespeichert.

Das hat den Vorteil das mehrer Worker Instanzen verteil laufen können und das, das Ergebnissen nicht beeinflusst wird. 

## Decision Tree

Ein Decision Tree besteht aus Knoten die mit gewichteten Ästen verbunden sind. Jeder Knoten hat einen Wert zwischen 0 und 1.
Knoten mir Kindern haben keine Datenquelle sondern berechnen ihren Wert anhand der werte ihrer Kinder und deren Gewichtungen. Endknoten haben eine Datenquelle welche eine Funktion, ist die einen Wert zwischen 0 und 1 zurück gibt.

So können viele Datenpunkte mit unterschiedlicher Wichtigkeit zusammen gerechnet werden. 

![Visuelle Darstellung eines einfachen Entscheidungsbaumes](media/DecisionTree.png)

```yaml
channel:
    weight: 100
    count: # How many messages are in the Channel
        weight: 10
        all: 30
        1d: 10
    participation: # How much the users writes
        weight: 50
        all: 50
        1d: 60

contact:
    weight: 100
    count:
        weight: 50
        all: 60
        1d: 30
    frequency:
        weight: 0
        1d: 70
        7d: 20

message:
    weight: 100
    tokenizer:
        weight: 90
        mentions: 70 # If the message contains a Contact name
        sentenze_marks: 50 # If the message contains a ! or ?
    datetime: 50 # time of day and Day of week
    length: 10 # How many Characters/ Words are in the message
```

Das ist ein Ausschnitt einer Konfigurationsdatei für den Worker. In ihr sind alle Knoten und deren Gewichtungen definiert. 