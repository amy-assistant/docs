\newpage

# Herausforderungen

Während der Entwicklung sind auf viele Hindernisse gestoßen, die wir alle Überwinden konnten. Eines der größeren Hindernisse war das finden der Applikationsstruktur. Unterstehenden ist das alte Architektur Diagramm von Amy.

![Alter technischer Aufbau von Amy](./media/AmyStructureOld.svg)

Die ursprüngliche Idee hinter der Server Struktur war es ein modulares, skalierbares und robustes System zu kreieren. Um das Umzusetzen wurde der Server Code auf verschiede Module aufgeteilt. Jedes Modul hat dabei eine eigene Aufgabe. 

### Core
Der Core ist wie schon der Name sagt das Herzen der Applikation, er startet alle anderen Module. Falls Module überlastet sind startet er zusätzliche Instanzen und im Falle eines Ausfalls die Module neu.

### Plugin
Das Plugin ist mit einem social Media Service verbunden. Es empfängt und sendet Nachrichten.

### Worker
Empfange Nachtrichten werden vom Worker mithilfe von Machine Learning analysiert und in die Datenbank gespeichert. 

### Api
Die Api ist die kommunikations Verbindung mit dem Client. Empfangene Nachrichten werden an den Client weitergeleitet und gesandte Nachrichten vom Client werden an das Plugin gesendet.

### Client
Grafisches Userinterface in Form von einer (Web) App.

### Datenbanken
In dieser Struktur haben wir verschiedene Datenbanken verwendet.
Für das Speichern von Usern wählten wir eine Postgres Datenbank. Denn Tabellen basierte Datenbanken eignen sich gut für gleichbleibende Datenstrukturen. Eine MongoDB verwenden wir für das Speichern von Nachrichten und eine RabbitMQ für das weiterleiten von Nachrichten.

## Problematik 
Im Laufe der Umsetzung bemerkten wir jedoch, dass diese Struktur zu komplex und aufwendig ist. Ein großes Problem war der Core. Ein Script zu schreiben, welches die verschiedenen Container überwacht ist herausfordernd und solange das Script nicht stabil läuft haben wir kein Grundgerüst auf der wir aufbauen können. Die Struktur war einfach zu komplex.

Des weiteren hatten wir Probleme mit MongoDB. Unsere Idee war gewesen, sobald ein neuer Eintrag in die Datenbank gespeichert wird, triggert das einen Event in der API, welche die Nachricht zum Client sendet. MongoDB unterstützt Trigger jedoch nicht.

Diese Probleme haben dazu geführt die Architektur von Amy neu zu überarbeiten. Das Ergebnis ist eine einfachere Grundstruktur, die im nächsten Kapitel beschrieben ist.
