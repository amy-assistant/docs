### VueJS
VueJs wurde für die Umsetzung des Userinterfaces verwendet. Es ist ein opensource Javascript Frontend Framework um Web Applikaiton zu erstellen. VueJS hat dabei ein großes Ökosystem, welches auch stark von den machern unterstützt wird. So gibt es einen Router, State Manager und CLI. Mit der CLI kann man  

Für die bestmögliche Umsetzung des Clients haben wir VueJS verwendet. Wie der Name schon verrät handelt es ich um ein Javascript Framework, zum Erstellen von User Interfaces verwendet wird. Es gibt auch noch andere Alternativen zu Vue, das wären Angular oder React. VueJS ist dabei das neuste Framework von den dreien und wurde von Evan You (尤雨溪) ins Leben gerufen. Als ehemaligen Angular Entwickler kennt er dessen und Reacts Schwächen und baut darauf auf. Vue enthält deshalb auch ein paar der besten Aspekte von Angular und React. Im Gegensatz zu der Konkurrenz ist es simpler, kompakter und relativ flexible, dass hält auch die Lernkurve für Anfänger flach, die gute Dokumentation unterstützt das zusätzlich.

**Button component in Angular**
```javascript
import { Component, Input } from '@angular/core';
 
@Component(
    selector: 'my-button',
    template: `
        <button type="button">{{title}}</button>
    `
)
export class MyButton {
    @Input() title: string;
} 
```

**Button component in React**
```javascript
import React, { Component } from 'react'
 
export MyButton extends Component {
  render() {
    const { title } = this.props
 
    return (
      <button type="button">{title}</button>
    )
  }
}
```

**Button component in VueJS**
```javascript
Vue.component('my-button', {
  template: '<button type="button">{{title}}</button>',
  props: ['title'],
})
 
new Vue({
  el: '#container'
})
```

VueJS hat ein sehr gutes und großes Ökosystem, welches auch die offiziellen Entwickler zusätzlich erweitern. So gibt es eine Konsolenanwendung "@vue/cli", welche einem erlaubt ein Gerüst für eine Vollwerte Applikation zu erstellen. 

```python
├── README.md
├── babel.config.js
├── node_modules
├── package.json
├── public
│   ├── favicon.ico
│   └── index.html
├── src
│   ├── App.vue #Vue File
│   ├── assets
│   │   └── logo.png
│   ├── components
│   │   └── HelloWorld.vue #Vue File
│   └── main.js
└── yarn.lock
```

Nach der Erstellung eines Vue Projektes fällt, auf das es Dateien mit einer neuen Dateiendung gibt. Wie der Endung der Datei `.vue` schon verrät handelt es sich um ein Vue File, um genauer ein Vue Component. VueJS ist Component basierend und profitiert von der Aufteilung der Applikation Logik. Components haben mehrere Vorteile, User können Components wiederverwenden, daher muss für dieselbe Tätigkeit keinen Code doppelten geschrieben werden. Components wieder zu verwenden hat auch einen Performance Technischen Vorteil, da VueJS diese nicht nochmal rendern muss.

*components/Contact.vue*
```html
<template>
  <div class="contact" @click="$router.push(`/chat/${id}`)">
    <avatar :src="img"/>
    <div id="wrapper">
      <span class="namespan text">{{name}}</span>
      <span class="subtext">{{subtext}}</span>
    </div>
  </div>
</template>

<script>
import { Avatar } from ".";

export default {
  name: "Contact",
  props: { id: String, name: String, img: String, subtext: String },
  components: { Avatar }
};
</script>

<style lang="scss">
.contact {
  background: $white;
  width: 100%;
  height: 4em;
  padding: 1.5em 1em;
  display: flex;

  ... style for .namespan .subtext #wrapper
}
</style>
```

Vue Components behalten immer Html, Javascript und Css. Mit Html im `<template>` Tag wird die Grundstruktur des Components definieret. Die Logik des Components wird in dem `script` Tag geschrieben und das Styling wird im `<style>` Tag festgelegt. Components können auch weitere Components beinhalten. In dem obigen Beispiel wäre dies das Component "Avatar", welchem ein Bild übergeben wird. Damit man das machen kann muss man zu seinem Component Properties (Props) geben, bei diesem Component wären es id, name, img und subtext und alle vom Typ String. 

Je großer die Applikation wird desto unübersichtlicher und anstrengender wird es die Logik der Components zu managen, da man immer in das passende File springen muss. Ein weiteres Problem ist, das man in Vue schlecht Variablen an das überliegende Components übergeben kann. Die Vue Cli unterstützt Plugins welche die Features der Applikation erweitern und von Vue gibt es eine eigene Lösung dafür. Das Plugin heißt Vuex und ist eine State Manager. Das bedeutet alle Variablen und Funktionen landen im Vue Store (State Manager), können von dort verwalten werden und jedes Component kann darauf zugreifen. 

Ein weites Problem was ein Plugin löst ist die Navigation durch die App. Der Vue Router löst das Problem elegant, da die verschiedenen Pfade als JSON Struktur definiert werden.  

*reduzierter Ausschnitt*
```javascript
const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [{
    path: '/login',
    name: 'Login',
    component: Login,
  }, {
    path: '/',
    component: Overview,
    children: [
      {
        path: '',
        name: 'Amy Assistant',
        component: Home,
      }, {
        path: '/contacts',
        name: 'Contacts',
        component: Contacts,
      },
      ...
    ]
  },
  ...
  ]
})
```

Zu einer Route gehört dabei immer ein Component (View) welche die anzuzeigenden Compontents beinhaltet. 