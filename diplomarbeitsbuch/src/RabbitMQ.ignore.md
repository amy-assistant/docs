


# RabbitMQ

RabbitMQ ist eine Open Source Message Broker Software, die das Advanced Message Queuing Protocol (AMQP) implementiert. Der RabbitMQ-Server ist in Erlang geschrieben.

Eine Queue hat im Vergleich mit einer REST Api oder Unix Sockets Vorteile wie zum Beispiel Buffering, bi-direktionale Kommunikation und remote Clients.

### Message
Eine Message beinhaltet neben dem Content noch einen Header welcher Information wie zum Bespiel die Return Queue oder den content_type beinhalten kann. Im Content sind dann die Daten als string kodiert, dieser wird dann vom client in den angegeben content_type dekodiert.

### Producer
Sendet Messages an Exchanges.

### Exchange
Die default exchange sendet Messages basierend auf dem Routing Key in die richtige Queue

### Queue
Speichert Nachrichten nach dem FiFo (FirstInFirstOut) Prinzip. 

### Consumer
Bekommt die Nachrichten von der Queue.


![](media/exchanges-bidings-routing-keys.png)

<!-- 
## Libraries

### kombu (python)

### jackrabbit (nodejs)
-->