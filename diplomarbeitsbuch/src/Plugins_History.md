## Konzept Plugins

### Bewegung

Die Idee hinter den so genanten Plugins ist es eine Umgebung zur verfügung zu stellen mit welchem dann die einzelnen Clients für unter anderem Telegram und Facebook Messenger entwickelt werden.

Die Vorteile an diesem system sind das Alle Nachricht die in unser system kommen ein einheitliches format haben und nur ein mal der Code für Queue Verbindung und multi threading geschrieben werden muss.

Weitergehend können da durch Third Partys einfacher Plugins entwickeln was unser Öko system stärkt.


### Die RESTlichen Anfänge

Der erste Ansatz bestand darin nur eine Endpoint Spezifikation zur verfügung zu stellen welchen dann jedes Plugin implementieren muss.


![[Source](https://gitlab.com/amy-assistant/plugins/spec/blob/d888feea37629134cd422c0a927840be45f2142f/README.md)](media/plugin_spec.png)


Der Gedanke hierbei war das jedes Plugin völlig unabhängig implementiert werden kann. Da durch hätte aber unser System wenig Kontrolle darüber was die plugins machen und jedes Plugin braucht ein große Boilerplate um simple Funktionen zu implementieren.

### Die Postgres View

Um die Plugins stärker ins system zu integrieren gab es die Idee plugin sowie User Daten in einer SQL Datenbank zu speichern und den Plugins dann über das Posgres User und View system nur zugriff auf die Notwendig Daten gibt. Für die Kommunikation wollte ich das Posgres Notify system verwenden welches einem erlaubt Code auszuführen wenn sich in der Datenbank etwas ändert.

Da es sehr viel Boilerplate benötigte um die Listener zu konfigurieren und Sich mit dem Richtigen user einzuloggen, wurden eine Python Bibliothek erstellt die es vereinfachte Plugins für die diversen services zu erstellen. 

> Das Konzept der Amy Library wurde aber übernommen und ist auch jetzt Bestandteil der Plugin implementation [pypi/amy](https://pypi.org/project/amy/)

Die Probleme Hierbei sind das wir zwei Datenbanken bräuchten und das die Bibliothek sehr komplex und Fehler anfällig wurde desto mehr Funktionen wir benötigten.