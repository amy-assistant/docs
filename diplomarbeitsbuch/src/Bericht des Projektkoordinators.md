# Bericht des Projektkoordinators

## Zusammenarbeit im Team

In einem Team ist es von äußerster Wichtigkeit, dass die Mitglieder miteinander auskommen und einen Teamgeist bilden. Zuverlässigkeit und Hilfsbereitschaft unter Kameraden ist ebenso einer der kritischen Aspekte die einem Team nicht fehlen dürfen. Da wir uns als Team schon lange kennen und wir schon mehrere Projekte miteinander absolviert, Probleme gelöst und vieles erreicht haben, war es leicht für uns einen funktionierenden Teamgeist zu bilden. Dieser ist sehr wichtig für die Aufgabenverteilung der einzelnen Mitglieder, da jeder ein anderes Spezialgebiet hat. In einem funktionierendem Team weiß jedes Mitglied welche die Stärken und Schwächen der Kameraden sind. So kann man sich untereinander schnell und einfach unterstützen, und Wissenlücken füllen.

Das Team anfangs Probleme mit der Kommunikation und Koordination. Diese Stolpersteine wurden aber bald aus dem weg geräumt. Durch eine Telegram Gruppe mit allen Teammitgliedern und Betreuer wurde das Problem der mangelnden Kommunikation gelöst. Ebenso wurde dadurch die Teamkoordination stabilisiert. Zudem behält das Team durch Versionskontrolle, Kanban Board und Stand-Up-Meetings die Übersicht aller Dateien und den Fortschritt von Amy. 

## Zusammenarbeit mit unserem Betreuer

Die Zusammenarbeit mit unserem Betreuer DI Robert Baumgartner lief seit Beginn des Projektes sehr gut. Unser Betreuer hat uns stets motiviert und angespornt. 
