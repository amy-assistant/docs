
# Konkurrenzanalyse

Bevor wir mit Amy begonnen haben, haben wir uns umgesehen welche Produkte es auf dem Markt gibt. Wir haben festgestellt, dass es bezüglich des Konzepts nur ähnlich wenige Produkte gibt. Keines der von uns gefunden Produkten verwendet künstliche Intelligenz zum Sichten und Filtern der Nachrichten. Somit ist dieses Feature von Amy ein wichtigstes Alleinstellungsmerkmal.  

## Rambox, Franz & All-In-One Messenger

Diese Produkte sind nur als Desktop Apps und unterstützen keine Smartphones. Diese Apps funktionieren nur als Container für andere Applikationen. Rambox, Franz und All-In-One Messenger verhalten sich wie Browser. Diese linken die User zur Webapp der ausgewählten Applikation und zeigen sie im Fenster an. Würde man jedoch beispielsweise in Chrome Fenster mit jeweils WhatsappWeb, Telegram und Facebook öffnen, hätte man dasselbe erreicht wie diese Apps. 

## IM+

IM+ fungiert nach dem Prinzip der vorherigen Konkurrenten wie Franz oder Rambox, als Browser für die Webapps der Applikationen. Jedoch nicht am Desktop sondern als App auf Android und iOS.

## Disa

Disa ist nur auf Android verfügbar.

## Amy Assistant

|                   |    Android     |        IOS         |      Desktop       |          künstliche Intelligenz         |
| :---------------- | :----------------: | :----------------: | :----------------:  | :----------------: |
| **AMY**           | X | X | X  | X |
| **Franz**         |   |   | X    |   |
| **Rembox**        |   |   | X    |   |
| **Disa**          | X |   |      |   |
| **IM+**           | X | X |      |   |
| **AIO-Messenger** |   |   | X    |   |
