### Progressive Web App (PWA)
Um so viele Geräte wie mögliche zu unterstützen, ist der Client eine PWA. Eine progressive Web App ist eine Webseite, welche sich wie ähnlich wie eine klassische App verhält. Das heißt ich kann sie auf meinem Smartphone installieren und offline benutzen. So eine App kann auch im Hintergrund Daten synchroneren und Benachrichtigen schicken.

Um eine existierende Webseite in eine progressive Webapp zu verwandeln braucht sie erstmal ein Webmanifest. In dieser Konfigurationsdatei sind verschiede Attribute gesetzt wie der Name, das Icon oder die Farbe der App. Nachdem der `<link>` tag gesetzt wurde kann die App mit jedem Kompatiblen Browser installiert werden.

```html
<link rel="manifest" href="/manifest.json">
```


```json
	"name": "Amy Assistant",
  "short_name": "Amy Assistant",
  "icons": [{
      "src": "/img/icons/amy-192x192.png",
      "sizes": "192x192",
      "type": "image/png"
    },
    {
      "src": "/img/icons/amy-512x512.png",
      "sizes": "512x512",
      "type": "image/png"
    }
  ],
  "start_url": "/index.html",
  "display": "standalone",
  "background_color": "#ffffff",
  "theme_color": "#ffffff"
```
*Abbildung vom Amy Manifest*


Neben dem Webmanifest sind Service Worker essentiell für eine Progressive Web App. Der Service Worker erlaubt es Push Notifications zu senden und auch einen erweiternden offline Support zu implementieren. 

Was wichtig bei einem Service Worker zu wissen ist, er kann das DOM nicht manipulieren, läuft unabhängig von der Webseite und funktioniert wie ein Network Proxy. 

Was bedeutet das? 

![Beispiel Service Worker](./media/ServiceWorker.svg)

Der Service Worker sitzt zwischen der Applikation und dem Netzwerk. Er entscheiden was mit den einkommenden Network Requests oder ausgehenden Response passiert. Wenn wir Beispielsweise eine Webseite zum ersten Mal laden kann der Service Worker Assets wie HTML Seiten, JavaScripts oder Stylesheets mithilfe der Cache Api cachen. Die Cache Api ist dabei ein Teil der Server Worker Api und ist perfekt für diese Aufgabe, da sie erlaubt Requests und dazu passende Response zu cachen. 

```javascript
    cache.put('/test.json', new Response('{"foo": "bar"}'));
```

Wenn dann nochmals derselbe Request ausgeführt, kann man im Cache nach der Response suchen, wenn nichts gefunden wird kann man auf einen normalen Network request zurückfallen. 

```javascript
    //Returns Request object 
    return cache.match(url)
```

Das Problem, was wir in diesem Beispiel haben, ist, dass sobald wir einen Network Request gemacht haben, landet er in dem Cache. Wenn derselbe Request weitere male ausgeführt wird, bekommen wir die Response immer aus dem Cache. Wenn sich die Daten auf dem Server verändern, wird der Client nie die aktuellen Daten bekommen. 

Es gibt dazu ein passendes Zitat.


Für Amy Assistant wird WorkBox verwendet welches ein Framework für die Service Woker Api (und Cachi Api) ist. WorkBox stellet dabei oft verwendete Code Strukturen zu Verfügung, welche einen Service Worker zu schreiben nicht als zu repetitiv macht. Workbox stellt für das Caching 3 verschiede Patterns zu Verfügung CacheFirst, CacheOnly, NetworkFirst, NetworkOnly und StaleWhileRevalidate. Wobei CacheFirst, NetworkFirst und StaleWhileRevalidate am interessantesten sind. StaleWhileRevalidate ruft den Request parallel über den Cache und das Network auf. Antwortet falls vorhanden mit der Response im Cache und speichert/updatet den Cache mit der Network Response. 

*Workbox Beispiel von Amy*
```javascript
workbox.routing.registerRoute(
    /\.(?:js|css)$/,
    workbox.strategies.staleWhileRevalidate({
        cacheName: 'static',
    })
);
```

In dem Beispiel von Workbox wird jede JavaScript Datei `.js` und Stylesheet `.css` in dem Cache "Static" mit der Strategie staleWhileRevalidate gecached.

