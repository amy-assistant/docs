\newpage

# Aktueller Stand

Da wir schon dem Ende der Diplomarbeit nähern, haben wir fast alle Features implantiert und befinden uns in der Phase der Fertigstellung. Für die finale Version fehlen nur noch kleine Fehlerbehebungen und die Veröffentlichung unserer Applikation.

# Projektplanung und Durchführung

Für die Projektplanung haben wir als Grundkonzept Scrum verwendet, jedoch auf die Bedürfnisse des Teams angepasst. Scrum haben wir aus dem Grund gewählt, weil es ein sehr gutes Werkzeug ist um ein sich ständig änderndes Projekt zu managen. Bei Softwareentwicklung ist das schon fast essentiell, da zu beginn die Anforderungen nicht zu 100% festgelegt sind. Am Anfang haben wir passende Userstories und Usecases für das Projekt ausgearbeitet, welches die Ziele unseres Projektes widerspiegeln. In dem Zeitraum der Diplomarbeit haben wir Meilensteine festgelegt. Für eine bessere Übersicht haben wir Scrum mit Kanban erweitert. Kanban ist ein Board wobei die Arbeitspakete einen Lebenszyklus von neuerstellt bis abgeschlossen durchleben. Wir haben so ein Board verwendet um unsere kurzfristigen Ziele und Bugs zu adressieren.

![Kanban Board aus GitLab](./media/board.png) 

## Aufgabenaufteilung

### Max Aussprung
ist Scrummaster und Programmierer. Seine Hauptaufgabe ist die Erstellung der API welche die Kommunikation mit dem Client (App) ermöglicht, dabei programmiert er auch Teile des Clients. 

### Ivan Ivelic
ist verantwortlich für die Auswertung der gesammelten Daten von Amy. Die ausgewerteten Daten werden von ihm visualisiert in auf einer Webseite angezeigt.

### Liam Perlaki
Ist der Productowner. Er ist verantwortlich für die Applikationsstruktur und die Filterung der Nachrichten.

### Matthias Pop Buia
Ist verantwortlich für das Design, Marketing des Projektes und programmiert den Userworkflow der App. 
