# Docker

Docker ist eine Application zur Containervirtualisierung.

Es vereinfacht die Bereitstellung von Anwendungen, da Container, die alle erforderlichen Pakete enthalten, einfach als Dateien transportiert und installiert werden können. Container stellen die Trennung und Verwaltung der auf einem Computer verwendeten Ressourcen sicher.

Grundsätzlich konzentriert sich Docker auf die Virtualisierung mit Linux. Docker kann jedoch auch über Hyper-V oder VirtualBox unter Windows und über VirtualBox unter macOS verwendet werden.

<!--
![Docker können verschiedene Schnittstellen verwenden, um auf Virtualisierungsfunktionen des Linux-Kernels zuzugreifen.](media/Docker-linux-interfaces.svg.png)
-->

### Image

beinhaltet alle Daten die für das Laufen der Applikation benötigt werden. Ein Image kann nach dem "builden" nicht mehr geändert werden. Es ist Portable und kann einfach über eine Registry auf andere Host Geräte verteilt werden.

### Container

ist eine gestartete Instanz einer Images. Der Container wird also gerade ausgeführt und ist beschäftigt. Sobald der Container kein Programm ausführt oder mit seinem Auftrag fertig ist, wird der Container automatisch beendet. Von einem Image kann man beliebig viele Container starten.


### Dockerfile

eine Textdatei, welche mit verschiedenen Befehlen ein Image beschreibt.

"FROM" definiert ein basis Image auf dem Aufgebaut wird. Mit "ENV" können Environment Variablen festgelegt werden. Build befehle können mit "RUN" ausgeführt werden. Durch "WORKDIR" wird das arbeite Verzeichnis festgelegt und über "COPY" können Dateien in das Image kopiert werden. "CMD" wird beim starten des Containers ausgeführt.

_Dockerfile von Worker_
```Dockerfile
FROM python:alpine

ENV AMY_DB_HOST='159.89.24.207'
ENV AMY_DB='amy'
ENV AMY_Q_HOST='159.89.24.207'

RUN pip install pymongo kombu pyyaml

WORKDIR /app

COPY weights.yaml weights.yaml
COPY amy_worker amy_worker

CMD [ "python", "-m", "amy_worker" ]
```



### Registry

eine Registry, wie zum Beispiel Docker Hub oder GitLab, dient der Verwaltung von Images.

![Docker Ablauf](media/docker-host.png)