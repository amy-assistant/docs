# CI/CD

Da alle Repositories auf GitLab liegen entschieden wir uns auch das GitLab tool GitLab CI für unsere Continuous Integration zu verwenden.

Um GitLab CI zu verwenden muss man nur eine `.gitlab-ci.yml` Datei im Stammverzeichnis des Repositories liegen haben. GitLab CI verwendet als basis Docker images


Hier die CI Datei um die Amy Assistant Webseite automatische auf den Web Server zu deployen wenn Änderungen auf den Master Branch des Repositories gepusht werden.

```yaml
image: alpine:latest

ipax:
  stage: deploy
  script:
  - apk --no-cache add lftp
  - lftp -c "set ftp:ssl-allow no; open -u $FTP_USERNAME,$FTP_PASSWORD $HOST; mirror -Rnev ./public . --ignore-time --parallel=10"
  only:
  - master

pages:
  stage: deploy
  script:
  - echo 'Nothing to do...'
  artifacts:
    paths:
    - public
  only:
  - master

```