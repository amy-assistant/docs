+++

# Projektidee
## Ausgangssituation
In der heutigen Zeit gibt es für jede Art von Mensch ein passendes soziales Netzwerk. Sei es das alt bekannte digitale und coole Freundebuch Facebook, welches viele Iterationen durchlebte und mittlerweile zu einem Eventplanner mutierte. Twitter, wo jeder frei wie ein Vogel seine Gedanken, Gefühle und Meinungen teilt. Instagram, Facebooks Rettungschiff um den funky und freshen Zeitgeist der Jugend einzufangen und in Form einer Bildergalerie freizulassen. Oder seien es klassische Chat Plattformen wie WhatsApp oder Telegram das bessere WhatsApp.

Diese Plattformen teilen sich trotz den Unterschieden eine gemeinsame Eigenschaft: Sie ermöglichen Kommunikation und den Austausch von Daten zwischen Personen. Wir, als Menschen finden es aufregend miteinander zu kommunizieren. Daher ist es kein Wunder, dass wir viele verschiede Apps soziale Netzwerke auf unserem Smartphone haben. 

Die Folge daraus ist, viele Menschen starren andauernd auf ihr Mobiltelefon. Wenn man seine Umgebung genau beobachtet, sieht man immer eine Person, welche in egal welcher Lebenssituation, ihre komplette Aufmerksamkeit dem Handy schenkt. Dieses Phänomen, kann man in Straßenbahnen, verschiedenen Restaurants, bei Personengruppen und sogar bei Liebespaaren beobachten. Wir kommunizieren mehr, gleichzeitig aber auch weniger.

<!-- Auf den Homescreens vieler Nutzer herrscht dabei Chaos. Man muss Ordner anlegen seine Social Media Applikation zu verwalten. Man kann es ihnen jedoch nicht übel nehmen, da Dienste wie Facebook zwei verschiede Apps brauchen um richtig zu funktionieren.  -->

In den meisten Fällen sind auch immer die selben Personen in den Freundeslisten der zahlreichen sozialen Netzwerke vertreten. Wenn man mit einer Person über verschiede Netzwerke chattet, ist das nicht nur unnötig komplex und führt zu Verwirrung.

Dazu kommen noch Pushnachrichten, die kaum wichtige Informationen beinhalten. Im schlimmsten fall sind es Hook damit man ein weiteres mal die Applikation auf dem Handy öffnet.


Weiters Belegen diese Apps auf dem Smartphone viel Speicherplatz. So verbraucht eine neu installierte Social Media App im durchschnitt 100MB. Im Laufe des Betriebes kommt diese auf das Doppelte.

## Kurze Beschreibung der Idee

Das Ziel von Amy Assistant ist die vorher genannten Probleme zu lösen.

Amy ist eine Smartphone App und eine dazugehöriger Server. Sie ersetzt die unterschiedlichen Apps der Social-Media Dienste und präsentiert eine einheitliche Oberfläche. Dadurch wird nicht nur die Bedienung erleichtert, man findet auch alle Nachrichten an einem Ort und der benötigte Speicherplatz am Smartphone wird stark reduziert. 

Amy arbeitet als persönliche Assistentin. Sie sichtet Nachrichten aus den Messaging Diensten, bei denen der User registriert ist und entscheidet mit Hilfe von künstlicher Intelligenz welche Nachrichten wichtige Information beinhalten und welche nicht, daher informiert sie den User nur wenn es umbedingt nötig ist. Amy hat alles gespeichert und wenn der User später nachsehen möchte, zum Beispiel ob auf Facebook ein Freund oder Freundin neue Nachrichten gesendet hat, dann kann er das natürlich auch über Amy machen. Amy lernt mit der Zeit dazu. Diese Feature von Amy erlaubt es dem User auf seine gerade Tätigkeit zu konzentrieren und sich nicht durch unwichtige Benachrichtigung unterbrechen zu lassen.

Mit Amy wollen wir einen Beitrag dazu leisten, dass wir wieder ein wenig Kontrolle über unser Leben zurückbekommen.