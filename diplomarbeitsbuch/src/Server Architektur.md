# Server Architektur

## Serviceorientierte Architektur
Bei einer Serviceorientierte Architektur wird die Applikation in einzelne Komponenten aufgeteilt die über ein Netzwerk Protokoll mit einander kommunizieren. 

Im vergleich zu einer monolithische Programmstrukturen bietet eine Serviceorientierte Architektur Flexibilität und einfache Skalierbarkeit außerdem erlaubt sie ein verteiltes laufen des Systems. 

## Skalierbarkeit

![Die Originalen Amys Services und wie sie kommunizieren](media/AmyArchitectureOld.svg)


## Simple

![Informationsfluss der neuen Architektur von Amy](media/AmyArchitectureNew.png)