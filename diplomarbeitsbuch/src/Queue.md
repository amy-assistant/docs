# Queue

Die Queue ist verantwortlich für die Kommunikation zwischen unseren Modulen.

Sie hat im Vergleich mit einer REST Api oder Unix Sockets Vorteile wie zum Beispiel Buffering, bi-direktionale Kommunikation und remote Clients.

## Broker/Server

Unser Queue Server ist die Open Source Message Broker Software RabbitMQ was gut ist da, dadurch keine Lizenz gebühren und andere Probleme durch proprietäre Software anfallen.  

RabbitMQ implementiert das Advanced Message Queuing Protocol (AMQP) für das es viele Clients in den meisten Sprachen gibt. Wir verwenden für unser Projekt "kombu" in Python und "jackrabbit" in NodeJS


### Message
Eine Message beinhaltet neben dem Content noch einen Header welcher Information wie zum Bespiel die Return Queue oder den content_type beinhalten kann. Im Content sind dann die Daten als string kodiert, dieser wird dann vom client in den angegeben content_type dekodiert.

### Producer
Sendet Messages an Exchanges.

### Exchange
Entscheidet in welche Queue die Message kommt, die default Exchange sendet Messages basierend auf dem Routing Key in die richtige Queue

### Queue
Speichert Nachrichten nach dem FiFo (FirstInFirstOut) Prinzip. 

### Consumer
Bekommt die Nachrichten von der Queue.


![RabbitMQ Broker Flow](media/exchanges-bidings-routing-keys.png)



## Json


## Work Queues

Machen es möglich das wir mehrer Worker starten können wenn die last für einen zu groß ist.

## Routing

Wir verwenden Routing um nur dem richtigen Plugin die Nachricht zu schicken.


## Remote Procedure Call

Ist die Grundlage für alle Aktionen die wir in einem Plugin ausführen wollen. So kann von die Api einem Plugin mitteilen das eine neue User Instanz zu erstellen ist und bekommt wenn das passiert ist einen Status code zurück.