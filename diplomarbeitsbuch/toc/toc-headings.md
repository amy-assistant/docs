# Table of Contents
# Einleitung
# Kurzfassung
# Abstract
# Ehrenwörtliche Erklärung
# Präambel
---
# Projektidee [x]
## Ausgangssituation [x]
## Beschreibung der Idee [x]

# verwendete Server Technolgien
## Python
## JavaScript
## MongoDB
## RabbitMQ
## Docker

# Serverarchitektur
Aufbau..
## Datenbank (MongoDB)
Aufbau der Datenbank...
## Queue (Queue Strucktur)
Kommikation zwischen den Modulen
## Docker (Container)
Wie verwenden wir Container

# Module
## Core (Prototype)
### FeatherJS
## Worker
### Dessisiontree

# Plugins
## Framework
## Telegram 
## Messenger


# Client
## Design [x]
### Stylesheet [x]
### Mockups [x]
### Scss [x]
## VueJs
### Components
### VueRouter
## Progressive WebApps [x]
### Webmanifest [x]
### ServiceWorkers [x]
## Interface

# Continues Deployments
# Continues Integration

# Marketing
## Social-media
## Print

# Statisken [x]

# Extra Content
# Projektmanagment
# Lessons Learned
# Quellenverzeichnis
# Anhang 
## Arbeitsaufteilung
## Gitlab Statistiken
## Diplomarbeitsantrag & Protokolle




