module.exports = collection => async context => {
  const db = await context.app.get("mongoClient");
  if (context.params.user)
    context.service.Model = db.collection(
      `${collection}.${context.params.user._id}`
    );
  return context;
};

let e = {
  _id: {
    $oid: "5c9f98cfd56ced19c607b84d"
  },
  email: "example@mail.com",
  password: "$2a$13$qwgK54GKTX/OuR7/Pjp3ruTAeuwLY.oeP/jqInSL7.4hVegiLWVqu",
  username: "example",
  platforms: {
    messenger: {
      session: "43f932e4f7c6ecd136a695b7008694bb69d517bd",
      username: "example@mail.com"
    },
    telegram: {
      username: "+43 123 456789"
    }
  }
};

let o = {
  _id: {
    $oid: "5ca1b85632f7304a1963f18c"
  },
  platform: "messenger",
  content: "Hallo",
  sender: "100006916442302",
  channel: {
    $oid: "5c9f9a19cb11281b4d41edcb"
  },
  datetime: {
    $date: "2019-04-01T05:05:57.000Z"
  },
  user: {
    $oid: "5c9f98cfd56ced19c607b84d"
  },
  out: false,
  importance: 0.9120852966473194
};

let b = {
  _id: {
    $oid: "5c9f9a19cb11281b4d41edcb"
  },
  name: "100006916442302",
  platforms: {
    messenger: "100006916442302",
    telegram: "571667900089701"
  }
};

let a = {
  name: "Amy Assistant",
  short_name: "Amy Assistant",
  icons: [
    {
      src: "/img/icons/android-chrome-192x192.png",
      sizes: "192x192",
      type: "image/png"
    },
    {
      src: "/img/icons/android-chrome-512x512.png",
      sizes: "512x512",
      type: "image/png"
    }
  ],
  start_url: "/index.html",
  display: "standalone",
  background_color: "#ffffff",
  theme_color: "#ffffff"
};

workbox.routing.registerRoute(
  /\.(?:js|css)$/,
  workbox.strategies.staleWhileRevalidate({
    cacheName: "static"
  })
);

workbox.routing.registerRoute(
  /^https:\/\/fonts\.gstatic\.com/,
  workbox.strategies.cacheFirst({
    cacheName: "google-fonts-webfonts",
    plugins: [
      new workbox.cacheableResponse.Plugin({
        statuses: [0, 200]
      }),
      new workbox.expiration.Plugin({
        maxAgeSeconds: 60 * 60 * 24 * 365,
        maxEntries: 30
      })
    ]
  })
);
