# Diplomarbeit
## 5CHITM – Reife- und Diplomprüfung 2018/19



| Gesamtprojekt | Amy |||
| -- | -- | -- | -- |
| Aufgabenstellung des Gesamtprojektes | Ziel der Diplomarbeit ist die Reduzierung der Informationsflut, die durch Social-Apps, E-Mail, Push-Nachrichten und andere Informationsquellen entsteht.  Dazu soll ein Input/Output-System entwickelt werden, welches jegliche Art von Input vereinheitlicht und anschließend daraus die Daten extrahiert. Aus den so gewonnen Daten entscheidet eine künstliche Intelligenz (Amy), ob diese relevant für den User sind oder nicht. Dabei lernt Amy dauernd mit und passt sich so dem User an. Weitere geplante Features sind einen Social-Graph zu erstellen, ein automatisches Antworten, ein automatischer Kalender und Reminder sowie ein Voice-Output falls Bluetooth Headphones verbunden sind. |||
| **Kandidaten / Kandidatinnen** || **Betreuer / Betreuerin** ||
| Max Aussprung || Prof. Robert Baumgartner ||
| Matthias Pop Buia || Prof. Robert Baumgartner ||
| Liam Perlaki || Prof. Robert Baumgartner ||
| Ivan Iveljic || Prof. Robert Baumgartner ||

+++

# Erklärung

Die Kandidaten / Kandidatinnen nehmen zur Kenntnis, dass die Diplomarbeit in eigenständiger Weise und außerhalb des Unterrichtes zu bearbeiten und anzufertigen ist, wobei Ergebnisse des Unterrichtes – als solche klar gekennzeichnet – mit einbezogen werden können.

Die Abgabe der vollständigen Diplomarbeit hat bis spätestens

### 9. April 2019

beim zuständigen Prüfer / der zuständigen Prüferin in ausgedruckter (2 Exemplare) und digitaler Form (CD-ROM, DVD, USB) zu erfolgen.

Kandidaten / Kandidatinnen | Unterschrift
--|--
Max Aussprung | |
Matthias Pop Buia | |
Liam Perlaki | |
Ivan Iveljic | |

<div style="display: flex; text-align: center; height: 120px; margin-top: 60px; font-size: 80%;">
<div style="align-self: flex-end; flex-basis: 33%; border-top: 1px black solid;">
**Mag. Thomas Angerer**<br>Abteilungsvorstand
</div>
<div style="align-self: flex-start;  flex-basis: 33%; border-top: 1px black solid;">
**Prof. Robert Baumgartner**
</div>
<div style="align-self: flex-end;  flex-basis: 33%; border-top: 1px black solid;">
**DI Peter Johannes Bachmair**<br>Direktor
</div>
</div>

---

## Genehmigung


<div style="display: flex; text-align: center; height: 60px; margin-top: 60px; font-size: 80%;">
<div style="flex-basis: 30%; border-top: 1px black solid;">
Ort, Datum
</div>
<div style="flex-basis: 20%;"></div>
<div style="flex-basis: 50%; border-top: 1px black solid;">
**LSI Mag. a Dr. in Brigitte Heller, MBA**<br>LandesschulinspektorIn
</div>
</div>


+++
# Inhaltsverzeichnis

{{TOC}}


+++
# 💡 Projektidee [Projektidee]


## Ausgangssituation


Es gibt zu viele Informationen, welche wir nicht auf einmal verarbeiten können. Daher möchten wir mit dieser Arbeit  Menschen helfen die Informationsflut zu strukturieren um das Leben zu vereinfachen. 


## Beschreibung der Idee


Viele Menschen leiden im heutigen Zeitalter an Informationsüberschuss. Jeder Mensch besitzt zu viele Social-Apps, sei es WhatsApp, Telegram, Twitter, Facebook, Instagram, Reddit, usw… Unser Anliegen ist es all diese verschiedenen Dienste und Streams in ein einheitliches Format zu vereinen und dann später mit Hilfe von AI weiter zu verarbeiten. Das ist unter anderem ein automatischer Kalendereintrag sowie auch eine automatische Antwort.  

+++ 
# 🏁 Projektziele [Projektziele]
    

## Muss Ziele
    

1. Es wird eine AI entwickelt, die Nachrichten analysiert und  trifft Entscheidungen basierend auf Inhalt und Relevant, ob die Nachricht weitergeleitet werden soll oder nicht.
2. Der Client ist eine progressive Web App welche Cross Platform verfügbar ist und die von AI ausgewählten Nachrichten anzeigt.
3. Die vom Server übermittelten Nachrichten werden verschlüsselt zur Verfügung gestellt.
4. Es wird eine Open Source Specification entwickelt, die definiert wie Plugins Messages/Events für den Server aussehen müssen.
5. Telegram, Twitter, WhatsApp werden als Referenz Plugins zur Verfügung gestellt.
6. Statistiken aus den gesammelten User Daten generieren.


## Optionale Ziele

1. Die AI kann einfache Fragen (Nachrichten) für den User beantworten.
2. Einen Audio Output für eingehende Nachrichten.
3. Es ist ein User Spezifischer Social-Graph generiert worden, welcher soziale Beziehungen zwischen Kontakten angezeigt. 
4. Es gibt Integrationen zu Kalender Services.
5. Es ist ein Teaser-Video für den Assistenten erstellt.
6. Größere Auswahl an Plugins.



## Nicht Ziele

1. Sprachsteuerung ist nicht vorgesehen.

+++
# 💣 Projektorganisation [Projektorganisation]
    

## Grafische Darstellung (Organigramm)

![Organigram](Organigram.svg)


## Projektteam
| Funktion | Name | Kürzel | E-Mail |
|--|--|:--:|--|
Projektleiter | Max Aussprung | 🐦 | aussprung794m@htl-ottakring.ac.at
Stellvertretender Projektleiter | Matthias Pop Buia | 🦇 | popbuia562m@htl-ottakring.ac.at
Projektmitarbeiter | Liam Perlaki | 🦊 | perlaki874l@htl-ottakring.ac.at
Projektmitarbeiter | Ivan Iveljic | 🐵 | iveljic247i@htl-ottakring.ac.at


## Individuelle Aufgabenstellung
    
#### Max Aussprung: Projektleiter, Programmierer
Der Projektleiter übernimmt die methodische Abwicklung des Projekts, vergibt die Aufgaben, achtet auf die Einhaltung der Fristen, führt die Kommunikation zwischen Projektcoach und dem Projektteam. Als Programmierer ist er verantwortlich für das Frontend und unterstützt den Hauptentwickler bei der Planung und Umsetzung des Back-ends(REST Api).

#### Liam Perlaki: Core Programmierer, Repository Admin
Hauptentwickler des Projektes, er programmiert den größten Teil (Backend) des Projekts und ist dafür verantwortlich, als Repository Admin ist er auch dafür verantwortlich, dass keine Daten des Projekts verloren gehen.

#### Matthias Pop Buia: Designer, Marketing Management
Als Corporate Designer übernimmt er das  Marketing, die Social-Media Präsenz und auch das visuelle Design des Produktes. Zusätzlich, hilft er bei der Umsetzung des Frontends.

####  Ivan Iveljic: Statistiker
Wertet und analysiert die gesammelten Userdaten und Serverdaten aus und präsentiert sie visuell. Definiert die Statistik-REST-API Spezifikationen.

+++
# 🌳 Projektumweltanalyse [Projektumweltanalyse]
    

## Grafische Darstellung
    
![Umweltanalyse](Umweltanalyse.svg)


## Beschreibung der wichtigsten Umwelten


| # |Bezeichnung || Beschreibung |||| Bewertung | 
|:--:|--|--|--|--|--|--|:--:|
1|Liam Perlaki||Projektteammitglied, Hauptentwickler, Admin des Repositorys||||+
2|Matthias Pop Buia||Projektteammitglied, Verantwortlich für Design, Marketing, Social-Media Präsenz||||+
3|Ivan Iveljic||Projektteammitglied, Verantwortlich für die Auswertung von Daten||||+
4|Max Aussprung||Projektleiter - Schnittstelle zwischen dem Team und dem Projektauftraggeber, Programmierer, Unterstützung bei der Social-Media Präsenz||||+
5|Prof. Robert Baumgartner||Projektbetreuer, kontrolliert den Projektstatus, gibt Tipps und Ratschläge, falls ein Problem vorliegt||||+
6|User||Dem Enduser muss das Produkt gefallen, damit eine Nutzung garantiert ist.||||~
7|Social-App Publisher||verschiedene Entwickler der Social-Apps||||~
    

+++
# ⚠️ Risikoanalyse [Risikoanalyse]
    

## Beschreibung der wichtigsten Risiken  



|#|Bezeichnung||Beschreibung des Risikos||||P|A|RF|
|:--:|--|--|--|--|--|--|:--:|:--:|:--:|
6|User||User beschweren sich über die Verarbeitung ihrer persönlichen Daten.||||30|30|1200
8|Know-How AI||Wir setzen uns das erste Mal mit diesem Thema auseinander.||||70|45|3150
9|Know-How Marketing||Wir haben noch nie eine Marketing Kampagne durchgeführt.||||20|10|200
10|Progressive WebApp||Wir haben noch nie eine PWA erstellt. Um alle Features zu unterstützen, brauchen wir low-level Zugriff auf das Mobile-OS.||||50|60|3000

 

	P...Eintrittswahrscheinlichkeit des Risikos
	A...Schadensausmaß bei Eintritt des Risikos
	RF...berechneter Risikofaktor


## Risikoportfolio

![Risikoportfolio](Risikoportfolio.svg)

## Risiko Gegenmaßnahmen



|#|Bezeichnung|Gegenmaßnahme|
|:--:|--|--|
6|User|Transparenz über die gespeicherten Daten und deren Nutzung/Weitergabe
8|Know-How AI|Tensor Flow Kurs besuchen und Internetrecherchen
10|Progressive WebApp|Auseinandersetzung mit Progressive WebApps

+++
# 💎 Meilensteinliste [Meilensteinliste]
    

Darstellung der Meilensteine mit geschätzten Terminen

| Datum | Meilenstein ||
|:--:|--|--|
4.6.2018  | Projekt Start||
20.9.2018 | Planung abgeschlossen||
30.9.2018 | Projektwebseite fertiggestellt und live geschaltet||
12.12.2018| AI implementiert und mit einem Plugin getestet||
16.1.2019 | Progressive Webapp fertiggestellt und mit AI verbunden||
19.1.2019 | Zwischenpräsentation||
2.2.2019  | Restliche Plugins fertiggestellt||
2.3.2019  | AI verfeinert und mit mehreren Usern getestet||
2.4.2019  | DA Buch fertiggestellt||
9.4.2019  | technische Abnahme||

  
# 💰 Kostenabschätzung [Kostenabschätzung]
    

Abschätzung der Kosten des Projekts

  

| # | Beschreibung der Kostenursache || Kosten
|:--:|--|--|:--:|
1|Hosting bei Digital Ocean||30€/mth
2|Mappe für den Antrag||1,30€
|Summe|||30€/mth & 1,30€


## Finanzierung
    

Das Budget wird vom Elternverein zur Verfügung gestellt.

  
# 🍭 Motivation [Motivation]
    

## Max Aussprung
> Ich möchte mit diesem Projekt etwas kreieren, dass bisher noch nicht da war. Dabei will ich mein, über die Jahre angeeignetes, Wissen auf die Probe stellen. Ich möchte mich weiterentwickeln und Lösungen zu Problemen finden, die ich möglicherweise haben werde im Laufe des Projektes. Da mich dieses Thema auch interessiert, bin ich äußerst motiviert mich damit zu befassen. Die Erfahrung, die ich mir hierbei aneignen werde, könnte mir in meinem zukünftigen Leben von Nutzen sein. 

## Liam Perlaki
> Ich bekomme über die unterschiedlichsten Kanäle alle möglichen Nachrichten und Updates zu jedem erdenklichen Thema, aber habe ich nicht die Zeit oder Lust mich mit den notwendigen Kleinigkeiten herumschlagen, die Mitmenschen und Bots von mir wollen. Ich erhoffe mir, dass unsere AI dabei Abhilfe schaft und Aufgaben, wie simple Antworten auf Nachrichten schreiben oder Kalendereinträge für erhaltene Termine, für mich übernimmt.

## Matthias Pop Buia
> Mein Handy benutze ich sehr häufig im täglichen Alltag. Während der letzten Jahre haben sich viele verschiedene Social-Apps angesammelt, von denen ich wichtige Benachrichtigungen erhalte. Mittlerweile  haben diese Infos überhand genommen, sodass ich nur schwer zwischen wichtigen und unwichtigen Benachrichtigungen unterscheiden kann. Mit dieser Diplomarbeit möchte ich mein Leben komfortabler gestalten können, um nicht selber die wichtigen Nachrichten herausfiltern zu müssen.

## Ivan Iveljic
> Ich möchte das Projektteam mit meinem Fähigkeiten Daten zu verarbeiten und Statistiken zu erstellen unterstützen. Ich hoffe ich kann so eine interessante Übersicht und Einblick in das Projekt zu schaffen.
