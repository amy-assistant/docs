pa:Robert Baumgartner
date: 19.9.2018
pm0: Max Aussprung
pm1: Liam Perlaki
pm2: Matthias Pop Buia
pm3: Ivan Iveljic
---
 
# MANAGEMENT SUMMARY
<span style="font-weight:bold; color:#ff0000 !important"> An [%pa] am [%date] </span>

Liegen etwas in Verzug, beeinflusst das Projekt jedoch nicht. 

<img src="/traffic-lights/green.svg" style="height:60px">

## Teammotivation

Projektmitglieder sind immer sehr motiviert, Projektleiter hat jedoch an Motivationsmangel gelitten. Mittlerweile ist er jedoch wieder motiviert eine großartige Diplomarbeit zu erschaffen.

## Erledigte Arbeiten 
|Bearbeiter|Tätigkeit|Dauer(h)|Status|
|---|---|---|---|
|[%pm1]|Git Repository erstellt|2|grün|
|[%pm1]|Management des Git-Repository|3|grün|
|[%pm2]|Logo erstellt|2|grün|
|[%pm0]|geweint|1/6|grün|


## Arbeiten der nächsten Woche
|Bearbeiter|Tätigkeit|Dauer(h)|
|---|---|---|
|[%pm1]|Management des Git-Repository|1|
|[%pm2]|Style-Guide erstellen|2|
|[%pm0]|Homepage erstellen|2|
|[%pm1] & [%pm0]| simples Prototyp-Backend(Server)|4|