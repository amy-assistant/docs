

# Technische Details
## Architektur

![Technischer Aufbau von Amy](./media/AmyStructure.svg)

Auf der rechten Seite ist der Client. Er ist die App, mit welcher die User interagieren. Wenn der User in der App (Client) eine neue Nachricht schreibt, wird diese an die API gesendet. Ist die Nachrichten bei der Api angekommen, erstellt diese ein neues Message Objekt und sendet es an das Plugin. Amy ist so flexible gestaltet, das Plugins für jedes Social Media Netzwerk entwickelt werden können. Zurzeit haben wir Facebook Messanger und Telegram Plugins entwickelt und weitere sind in Planung. Wenn wiederum Nachrichten von Plugins empfangen werden, analysiert diese zuerst der Worker nach Wichtigkeit und leitet sie schließlich an den Client. Dieser zeigt sie dem User an.

+++

## Client
### Interface - Grundlagen

Der Client wurde als Progressive Web App entwickelt und ist für Android, iOS und Desktop verfügbar.

Im folgenden soll eine kurze Übersicht über die Bedienung des Client gegeben werden.

![Menüpunkte Settings, Chat-Overview und Contacts](./media/InterfaceTabs.svg)

Nachdem der User sich bei Amy registriert und seine Messaging Dienste hinzugefügt hat, landet man in der Chatübersicht [Bild Mitte]. Hier sieht man seine letzen Chats. Der Button mit einem Papierflieger Icon erlaubt es ihm eine neue Konversation zu starten kann. Die drei Symbole am unteren Ende des Bildschirmrandes können angetippt werden und welchen zwischen den 3 wichtigsten Menüpunkten von Amy hin und her. Das sind die *Einstellungen* [Bild links], *Chatübersicht* und *Kontakte* [Bild rechts]. In den Einstellungen kann der User zusätzliche Accounts für Messaging Dienste hinzufügen, Features deaktivieren und aktivieren und visuelle Anzeigen verändern. 

Klickt der User auf einen bereits vorhanden Chat (siehe Bild Mitte, Lukas Baumhaus), startet ein Chat mit diesem Kontakt.

<!-- weiß und größer -->
![Beispiel Chat](./media/chats.svg)

Hier sieht man einen Beispielchat mit Lukas Baumhaus. Auf der linken Seite sieht man die eingehenden Nachrichten und auf der rechten Seite die ausgehenden Nachrichten. Neben der Nachricht gibt es auch immer ein Kontaktbild mit einem Punkt, welcher rechts oben sitzt und den Messaging Dienst symbolisiert. In diesem Bild sieht man zwei Farben, grün steht hierbei für WhatsApp und blau für Telegram. In Amy Assistant hat man die Möglichkeit, Chats von unterschiedlichen Plattformen mit derselben Person vereint darzustellen. Wenn man auf eine Nachricht antwortet, verwendet Amy automatisch den letzt verwendeten Messaging Dienst.

## Umsetzungs Aspekte
### Progressive Web App (PWA)
Um so viele Geräte wie mögliche zu unterstützen, ist der Client eine PWA. Eine progressive Web App ist eine Webseite, welche sich wie ähnlich wie eine klassische App verhält. Das heißt ich kann sie auf meinem Smartphone installieren und offline benutzen. So eine App kann auch im Hintergrund Daten synchroneren und Benachrichtigen schicken.

Um eine existierende Webseite in eine progressive Webapp zu verwandeln braucht sie erstmal ein Webmanifest. In dieser Konfigurationsdatei sind verschiede Attribute gesetzt wie der Name, das Icon oder die Farbe der App. Nachdem der `<link>` tag gesetzt wurde kann die App mit jedem Kompatiblen Browser installiert werden.

```html
<link rel="manifest" href="/manifest.json">
```


```json
	"name": "Amy Assistant",
  "short_name": "Amy Assistant",
  "icons": [{
      "src": "/img/icons/android-chrome-192x192.png",
      "sizes": "192x192",
      "type": "image/png"
    },
    {
      "src": "/img/icons/android-chrome-512x512.png",
      "sizes": "512x512",
      "type": "image/png"
    }
  ],
  "start_url": "/index.html",
  "display": "standalone",
  "background_color": "#ffffff",
  "theme_color": "#ffffff"
```
*Abbildung vom Amy Manifest*


Neben dem Webmanifest sind Service Worker essentiell für eine Progressive Web App. Der Service Worker erlaubt es Push Notifications zu senden und auch einen erweiternden offline Support zu implementieren. 

Was wichtig bei einem Service Worker zu wissen ist, er kann das DOM nicht manipulieren, läuft unabhängig von der Webseite und funktioniert wie ein Network Proxy. 


Für Amy Assistant wird WorkBox verwendet welches ein Framework für die Service Woker Api (und Cachi Api) ist. WorkBox stellet dabei oft verwendete Code Strukturen zu Verfügung, welche einen Service Worker zu schreiben nicht als zu repetitiv macht. Workbox stellt für das Caching 3 verschiede Patterns zu Verfügung CacheFirst, CacheOnly, NetworkFirst, NetworkOnly und StaleWhileRevalidate. Wobei CacheFirst, NetworkFirst und StaleWhileRevalidate am interessantesten sind. StaleWhileRevalidate ruft den Request parallel über den Cache und das Network auf. Antwortet falls vorhanden mit der Response im Cache und speichert/updatet den Cache mit der Network Response. 

*Workbox Beispiel von Amy*

```javascript
workbox.routing.registerRoute(
    /\.(?:js|css)$/,
    workbox.strategies.staleWhileRevalidate({
        cacheName: 'static',
    })
);
```

In dem Beispiel von Workbox wird jede JavaScript Datei `.js` und Stylesheet `.css` in dem Cache "Static" mit der Strategie staleWhileRevalidate gecached.

<!-- Push Notifcations -->