

# Allgemeines

### Schule:

HTL Ottakring <br>
Thaliastraße 125 <br>
A-1160 Wien <br>
Tel: +43 01 49 111-113 <br>
Fax: +43 1 49 111-199 <br>


### Projektgruppe:

**Maximilian Aussprung**

Herbeckstraße 72 <br>
1180 Wien <br>
E-Mail: aussprung794m@htl-ottakring.ac.at <br>


**Ivan Ivelic**

Märtstraße 149/19 <br>
1140 Wien <br>
E-Mail : iveljic247i@htl-ottakring.ac.at <br>


**Liam Perlaki**

Gersthoferstraße 112/2/14 <br>
1180 Wien <br>
E-Mail : perlaki874l@htl-ottakring.ac.at <br>


**Matthias Pop Buia**

Jedlersdorfer Pl. 5 <br>
1210 Wien <br>
E-Mail : popbuia562m@htl-ottakring.ac.at <br>

+++

### Projektbetreuer

Prof. DI Robert Baumgartner, MBA <br>
Mooswiesengasse 9B <br>
1140 Wien <br>
Tel: +43 664 929 1985 <br>
E-Mail: robert.baumgartner@htl-ottakring.at <br>


Kategorie: Engineering II <br>
Projektnummer: 	J190260 <br>
Projekttitel: Amy Assistant <br>

