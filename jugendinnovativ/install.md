brew install pandoc
brew install pandoc-citeproc
brew install librsvg python homebrew/cask/basictex
sudo tlmgr install collection-fontsrecommended
sudo tlmgr install titlesec
sudo tlmgr install framed